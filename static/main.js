jQuery(document).ready(function () {
    
    // Обработка формы, пробная отправка данных через AJAX
    /*jQuery("form#reg").submit(function () {

        var url = $(this).attr('action');
        var data = jQuery(this).serialize();

        jQuery.post(url + '/ajax', data,
                function (d) {
                    console.log(d);
                }
        , 'json');

        return false;
    });  */
    
    // Обработка событий html5 ИНВАЛИД на форме 
    // цель, измение стандартных сообщений от браузера.    
    jQuery("form#reg input").bind('invalid', function(event) {
        var message = "Поле обязательно к заполнению";
        if ($(this).attr('required') == 'required') {
            message = 'Поле ' + $(this).attr('placeholder') + ' обязательно к заполнению';           
            if ($(this).val() != "") {
                message = 'Поле должно соответствать формату ' + $(this).attr('pattern');
            }
            this.setCustomValidity(message);
        }        
    }).change(function(){
        try{
            this.setCustomValidity('')
        } catch(e) {
            
        }
    });     
    
});