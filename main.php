<?php
// все приложение работает в пространстве имен app
namespace app;

header("Content-Type: text/html; charset=utf-8");
session_start();

/* Подключение конфиграционного файла, структура стандартная ini */ 
$GLOBALS['config'] = parse_ini_file(dirname(__FILE__) . '/config.ini', true);

/***
 * Хорошо бы использовать какой нибудь php шаблонизатор, но в ТЗ, 
 * нет указания использовать, поэтому view и layout будут использовать php 
 * 
 * Так же, ещё бы хорошо использовать какой-нибудь ORМ, для работы с СУБД,
 * но мы ведь пишем свой FW будем реализовать нечто своё для абстракции
 * от внешний источников, тем более у нас пользуется CSV 
*/

/**
 * Обработчик автомотической загрузки классов
 * основан на namespace 
 * 
 * \app\controller\user трансформируется в <currentDir>/app/controller/user.php
 * и подключается через require_once
 * 
 * @param string $className
 */
function MAutoload($className) {       
    
    $fileClass = dirname(__FILE__) . '/' . str_replace("\\", "/", $className) . ".php";
    
    // Проверка, существует ли файл, если нет ошибка, в первой итерации для наглядной отладки
    if (file_exists($fileClass)) {                
        require_once $fileClass;
    } else {        
        exit("Не существующий вызов $className [ $fileClass ]");
    }
}

// регистрируем свой автозагрузчик классов
spl_autoload_register('app\MAutoload');

/**
 * @todo Необходимо сделать лучше, решение очень костыльное
 * @return array
 */
function route() {   
    $result = array('controller' => 'index', 'action' => 'index');
        
    $array_url = parse_url($_SERVER['REQUEST_URI']);     // интересует [path]    
    
    // убираем в начале и в конце /
    $array_url['path'] = trim($array_url['path'], '/');
    
    
    // Получаем массив значений в порядке употребления уровней,
    // controller/action/param 
    // admin/controller/action/param - в базовом контроллере будет проверка
    
    $param = explode("/", $array_url['path']);          
        
    if ($param[2] == 'admin') {
        $result['controller'] = isset($param[2]) ? $param[2] : $result['controller'];
        $result['action'] = isset($param[3]) ? $param[3] : $result['action'];
        $result['param'] = isset($param[4]) ? $param[4] : false;
        $result['admin'] = 'admin';
    } else {
        $result['controller'] = isset($param[1]) ? $param[1] : $result['controller'];
        $result['action'] = isset($param[2]) ? $param[2] : $result['action'];
        $result['param'] = isset($param[3]) ? $param[3] : false;
    }
    
    return $result;    
}
