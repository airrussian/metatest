<?php
/**
 * Description of index
 * @author Alexey Woronenko <airrussian@mail.ru>
 */
namespace app\controllers;

use app\helpers;

class index extends controller {
    
    public function index() {
        return $this->view->fetch('index');
    }
    
    public function page404() {
        return $this->view->fetch('404');
    }
    
}
