<?php
/**
 * Контроллер для работы с пользователями
 * @author Alexey Woronenko <airrussian@mail.ru>
 */

namespace app\controllers;
use app\helpers;

class menu extends controller {
    
    /**
     * Стандартный экшен, стандартного контроллера. 
     */
    public function main() {
        
        $menu = array(
            array(
                'url'   =>  'http://airrussian.ru/metatest/',
                'name'  =>  'Главная'
            ),
            array(
                'url'   =>  'http://airrussian.ru/metatest/client/reg',
                'name'  =>  'Регистрация'
            ),            
        );
        
        $user = helpers\user::getInstance();
        
        if ($user->user['id']) {
            $menu[] = array(
                'url'   =>  'http://airrussian.ru/metatest/client/index',
                'name'  =>  'Список клиентов'
            );
        }
        
        $this->view->assign('menu', $menu);        
        return $this->view->fetch("main");
    }
       
    
}