<?php

/**
 * Контроллер для работы с клиентами
 * @author Alexey Woronenko <airrussian@mail.ru>
 */

namespace app\controllers;

use app\models;
use app\helpers;

class client extends controller {

    /**
     * Обработка события регистрации пользователя
     * @return string
     */
    public function reg() {

        // Получает данные от формы, елси они конечно пришли
        $client = filter_input(INPUT_POST, 'client', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
               
        // Проверям есть ли данные от формы
        if (!empty($client)) {
            $clientModel = new models\client();            
            // Валидируем их
            if ($clientModel->validata($client)) {                                                
                try {
                    $clientModel->insert($client);
                } catch (Exception $ex) {
                    exit ($ex->getMessage());
                }               
            }
            
        }

        // Очень чешиться и колиться сделать helper.form,
        // для того избавиться от views.client.reg, а валидацию сделать 
        // универсальную для JS и PHP, что бы в HTML постоянно не лесть.      
        
        $this->view->assign('client', $client);
        $this->view->assign('action', 'http://airrussian.ru/metatest/client/reg');
        return $this->view->fetch('reg');
    }

    /**
     * Просмотр информации о пользователи. 
     * @todo реализовать!!!! срочно
     * @return string
     */
    public function view($id = false) {

        if ($id) {
            $clientModel = new models\client();
            $client = $clientModel->GetRow($id);
            $this->view->assign('client', $client);
            return $this->view->fetch('view');
        } else {
            //redirect("404");
        }
    }

    /**
     * Удаление клиента из базы
     * @todo реализовать, сейнеприменно! 
     * @return string
     */
    public function del($id) {
        if ($id) {
            $clientModel = new models\client();
            $client = $clientModel->delete($id);
            return helpers\router::redirect(array('controller' => 'client', 'action' => 'index'));
        } else {
            helpers\router::redirect(array('controller' => 'index', 'action' => 'page404'));
        }
    }

    /**
     * Выводит список пользователей
     */
    public function index() {
        $clientModel = new models\client();
        $countRecords = $clientModel->count();

        $paginator = new helpers\paginator(
                helpers\router::url(array('controller' => 'client', 'action' => 'index')), 
                $countRecords);                

        $clients = $clientModel->getList($paginator->offset(), $paginator->onpage());

        $this->view->assign('paginator', $paginator);
        $this->view->assign('clients', $clients);

        return $this->view->fetch("index");
    }
    

}
