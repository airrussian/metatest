<?php

/**
 * Базовый класс, для управления контроллерами 
 * @author Alexey Woronenko <airrussian@mail.ru>
 */

namespace app\controllers;

use app\views;
use app\models;
use app\helpers;

abstract class controller {

    public $view;

    public function __construct() {
        helpers\user::getInstance();                
        $this->newTpl();
    }

    /**
     * 
     */
    private function newTpl() {
        $this->view = new views\tpl();
        $this->view->tmplPath = str_replace(array(__NAMESPACE__, "\\"), "", get_class($this));
    }
    
    public function execute($action) {
        
    }

}
