<?php

/**
 * Контроллер для работы с пользователями
 * @todo запилить регистрацию пользователей и ACL
 * @author Alexey Woronenko <airrussian@mail.ru>
 */

namespace app\controllers;

use app\models;
use app\helpers;

class user extends controller {

    /**
     * Авторизация пользователя     
     */
    public function auth() {            
        $formdata = filter_input(INPUT_POST, 'user', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);                        
        $userAuth = helpers\user::getInstance();
        if ($userAuth->authorization($formdata)) {            
            helpers\router::redirect();
        }
        return $this->view->fetch('auth');
    }
    
    /**
     * Информационная панель о пользователи.     
     */
    public function bar() {
        $user = helpers\user::getInstance();                
        $this->view->assign('user', $user->user);
        return $this->view->fetch('bar');
    }

    
    /**
     * Выход пользователя из системы
     */
    public function logout() {
        $userAuth = helpers\user::getInstance();
        if ($userAuth->logout()) {
            helpers\router::redirect();
        }
    }

}
