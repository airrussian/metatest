<?php

/**
 * Реализует постраничную навигацию 
 * @author AIR
 */

namespace app\helpers;

class paginator extends helper {
    
    private $class = 'pagination';
    private $countRecord = 1;
    private $current = 1;
    private $onpage = 5;
    private $countPages = 3;
    private $url;
    
    public function __construct($url, $countRecord, $onpage = 5) {
        
        $this->url = $url;        
        $this->countRecord = $countRecord;
        $this->onpage = $onpage;
        
        $page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);
        $this->current = is_numeric($page) ? $page : 1;
        
        /**
         * @todo сохраниять onpage в COOKIE или SESSION 
         */
        
        $this->countPages = ceil($this->countRecord / $this->onpage);
    }
    
    /**
     * Возращает какое должно быть смещение вывода записей из учета, 
     * количество записей выводимых на страницу
     * @return int
     */    
    public function offset() {
        return ($this->current - 1 ) * $this->onpage;
    }
    
    /**
     * Возращает сколько записей должно выводиться на страницу
     * @return int
     */
    public function onpage() {
        return $this->onpage;
    }

    /**
     * Выводит пагинатор
     * @return string html
     */
    public function show() {
        $result = "<ul class=\"{$this->class}\">";
        
        $result.= "<li><a href=\"{$this->url}\">&laquo;</a></li>";
        for ($p = 1; $p <= $this->countPages; $p++) {                        
            $result.= "<li><a href=\"{$this->url}?page=$p\">$p</a></li>";
        }
        $result.= "<li><a href=\"{$this->url}?page=last\">&raquo;</a></li>";
        $result.="</ul>";
        return $result;
    }

}
