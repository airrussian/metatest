<?php

/**
 * Description of router
 *
 * @author AIR
 */
namespace app\helpers;

class router extends helper {
            
    /**
     * Выполняет переадресацию на другую страницу
     */
    public static function redirect($page = "", $code) {
        header("Location: http://airrussian.ru/metatest");
        exit();
    }
    
      
    public static function url($to) {
                
        $url = "http://airrussian.ru/metatest";
        
        if (isset($to['controller'])) {
            $url .= "/" . $to['controller'];
        }
                
        if (isset($to['action'])) {
            $url .= "/" . $to['action'];
        }
        return $url;
    }
    
}
