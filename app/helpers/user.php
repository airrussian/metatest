<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author AIR
 */

namespace app\helpers;

class user {

    protected static $_instance;
    
    public $user;
    
    private $user_agent;
    
    private $remote_addr;
    private $salt;
    
    private $keysite = 'mgqcahb52rb57ivoa37p313b44';
    
    private $users_db = array(
        array(
            'id' => 1,
            'login' => 'admin',
            'pass' => 'admin'
        )
    );

    private function __construct() {
        
        /**
         * @todo вынести в session
         */        
        // Идентификатор сессии PHP 
        $this->session_id = session_id();
        // Браузер пользователя
        $this->user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "";
        // HTTP_X_REAL_IP у меня на серваке nginx выставляет эту переменную 
        $this->remote_addr = isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] :
                (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "");
        // Соль
        $this->salt = md5($user_agent . $remote_addr . $this->keysite);
                
        /**
         * Костыль для $users pass в кодированный
         * @todo Пользователей в БАЗУ или ещё куданибудь
         */
        foreach ($users as &$user) {
            $user['pass'] = md5($user['pass'] . $this->keysite);
        }
        
        self::recovery();                
    }
    
    /**
     * Проверяет авторизован ли пользователь
     */    
    private function recovery() {              
        // Если существует такая сессия
        if (isset($_SESSION[$this->session_id])) {
            // если существует сессионная кука
            if (isset($_COOKIE[$this->session_id])) {
                // и сессионная кука правильная
                if ($_SESSION[$this->session_id]['key'] == $_COOKIE[$this->session_id]) {                    
                    // тогда находим соответствующего пользоваля 
                    $user_id = $_SESSION[$this->session_id]['user_id'];                    
                    $this->user = $this->Get($user_id);                    
                } else {
                    throw new \Exception("It's hack");
                }
            } 
        } 
    }
    
    /**
     * Авторизует пользователя с указанным login pass
     * @example array('login'=>'admin', 'pass' => 'admin')
     * @param array $user
     * @return boolean true - в случае успеха
     */    
    public function authorization($user) {
        if (is_array($user)) {             
            $this->user = $this->find($user);            
            if (!empty($this->user)) {
                $_SESSION[$this->session_id] = array(
                    'user_id'   =>  $this->user['id'],
                    'key'       =>  md5($this->user_agent . $this->remote_addr . $this->keysite)
                );                        
                setcookie($this->session_id, $_SESSION[$this->session_id]['key'], time() + 100000, '/');
                return true;
            } else {
                return false;
            }
        }        
    }

    private function Get($id) {
         foreach ($this->users_db as $u) {
            if ($u['id'] == $id) {                
                return $u;
                break;
            }
        }
        return false;
    }
    
    public function toArray() {        
        return $this->user;
    }

    private function find($user) {        
        foreach ($this->users_db as $u) {
            if (($u['login'] == $user['login']) && (md5($u['pass'] . $this->keysite) == md5($user['pass'] . $this->keysite))) {
                return $u;
                break;
            }
        }
        return false;
    }
    
    public function logout() {
        setcookie($this->session_id, "", time() - 10000, "/");
        return true;
    }
    
    /**
     * Ну нельзя клонировать синглтон
     * @return boolean
     */
    private function __clone() {
        return false;
    }
    
    /**
     * Создаем синглтон
     * @return type
     */
    public static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }

}
