<?php

/**
 * Модель для работы с пользователями
 * @author Alexey Woronenko <airrussian@mail.ru>
 */

namespace app\models;

class client extends model {

    /**
     * Описание полей модели
     * @var array
     */
    protected $fields = array(
        'firstName' => array(
            'type' => 'string',
            'require' => true
        ),
        'secondName' => array(
            'type' => 'string',
            'require' => true
        ),
        'birthDay' => array(
            'type' => 'date',
        ),
        'company' => array(
            'type' => 'string',
        ),
        'post' => array(
            'type' => 'string'
        ),
        'telephone' => array(
            'type' => 'telephone',
            'require' => true,
            'unique' => true
        ),
        'id'    =>  array(
            'type'  =>  'autoinrement',            
        )
    );

    /**
     * Получает запись с идентификатором $id
     * @param integer $id
     * @return array
     */
    public function GetRow($id) {
        $this->source->read();        
        $result = $this->source->find(
            array(
                'fields'    =>  
                    array('firstName', 'secondName', 'birthDay', 'company', 'post', 'telephone', 'id'), 
                'where'     =>  array(
                    'id'    =>  $id
                ),
                'limit' => 1
                )
        );
        
        $result = reset($result);
        
        return $result;
    }

    /**
     * @todo Это должно уехать в основную модель!!! описание чего и как должно быть в переменной $this->validata;
     * @param array $data
     * @return boolean
     */
    public function validata(&$data) {        
        $result = true;
        
        if (empty($data['firstName']['value']) && ($data['firstName']['value'] == "")) {
            $data['firstName']['error'] = 'Поле "Имя" должно быть заполнено';            
            $result = false;
        }
        
        if (empty($data['secondName']['value']) && ($data['secondName']['value'] == "")) {
            $data['firstName']['error'] = 'Поле "Фамилия" должно быть заполнено';            
            $result = false;
        }
        
        if (!empty($data['birthDay']['value']) && ($data['birthDay']['value'] != "") ) {                        
            if (!preg_match('~\d{2}\.\d{2}\.\d{4}~si', $data['birthDay']['value'])) {
                $data['birthDay']['error'] = 'Поле "Дата рождения" должно иметь формат dd.mm.YYYY';
                $result = false;
            }            
        }
        
        if (empty($data['telephone']['value']) && ($data['telephone']['value'] == "")) {            
            $data['telephone']['error'] = 'Поле "Телефон" должно быть заполнено';
            $result = false;            
        } else {           
            if (!preg_match('~\+7\d{5,}~si', $data['telephone']['value'])) {                
                $data['telephone']['error'] = 'Поле "Телефон" должно иметь формат +7xxxxxxxxxx';
                $result = false;
            }
        }
                
        return $result;
    }
    
    public function insert($data) {        
        var_dump($data);        
        parent::insert($data);
        
    }

    public function getList($offset = 0, $limit = 10) {
        $this->source->read();
        return $this->source->find(
            array(
                'fields'    =>  
                    array('firstName', 'secondName', 'birthDay', 'company', 'post', 'telephone', 'id'), 
                'limit' => array($offset, $limit)
                )
        );
    }
    
}
