<?php

/**
 * Базовая модель, должна предоставлять абстрактную запись 
 * и чтение данных из любого источника данных, для которого 
 * есть адаптер.
 * 
 * @todo !но прослойка ещё до конца не готова, работает через костыли 
 * 
 * @author Alexey Woronenko <airrussian@mail.ru>
 */

namespace app\models;

use app\adapters;

class model {

    /**
     * Хранит источник данных 
     * @var array 
     */
    protected $source;

    /**
     * Подружает прир создании модели нужный источник хранения данных
     * Тема адаптера как паттерна, не раскрыта до конца. 
     */
    public function __construct() {
        $this->_config = $GLOBALS['config'];

        // Подключаем соответствующий адаптер
        if ($this->_config['db']['adapter'] === 'csv') {

            // Имя таблицы будет считатся название класса модели 
            $_table = str_replace(array(__NAMESPACE__, "\\"), "", get_class($this));
            // Если в модели указано свойство _table,             
            if (isset($this->_table)) {
                //  то название таблицы считается этот параметр            
                $_table = $this->_table;
            }
            // 
            $this->source = new adapters\csv(
                    $this->_config['path']['base'] .
                    $this->_config['db']['path'] .
                    "/" . $_table . '.csv');
        }
    }

    /**
     * Предполагается, написание универсального метода, для валидации, данных,
     * по заданной структуре ввиде массива в свойстве $validata
     * 
     * @todo NO RELEASE!
     * @param type $data
     * @return type
     */
    public function validata($data) {
        return true;
    }
    
    /**
     * Вставляет данные 
     * @param array $data
     */
    public function insert($data) {        
        $this->source->insert($data);
        $this->source->write();
    }

    /**
     * Обновляет данные
     * @param array $data
     */
    public function update($data) {
        $this->source->update($data);
        $this->source->write();
    }

    /**
     * Удалает данные
     * @param integer $id
     */
    public function delete($id) {
        $this->source->delete($id);
    }

    /**
     * Возращает количество записей в таблице (файле)
     * @return integer
     */
    public function count() {
        $this->source->read();
        return $this->source->count();
    }

}
