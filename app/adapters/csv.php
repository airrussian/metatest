<?php

/**
 * Адаптер для записи в CSV вместо таблицы базы данных
 * 
 * Фаилы, хранит записи в обратном порядке, для того, что бы 
 * более эффективно извлекать последний ID записи
 * 
 * @author Alexey Woronenko <airrussian@mail.ru>
 */

namespace app\adapters;

class csv {

    /**
     * Хранит источник данных
     * @var type 
     */
    private $_filename;

    /**
     * Хранит данные всего фаила в памяти
     * @todo плохо конечно, если фаил громадный будет, на первой итерации сойдет
     */
    private $source;

    /**
     * Проверяет существует ли указанный фаил
     * @param string $filename
     * @throws \Exception стандартное исключение
     */
    public function __construct($filename) {
        if (file_exists($filename)) {
            $this->_filename = $filename;
        } else {
            throw new \Exception('Фаил CSV c именем ' . $filename . ' не найден');
        }
    }
    
    /**
     * Читает из фаила данные, возвращает двух мерный массив (таблица),
     * Строка таблицы - строчка из фаила.      
     * @return array2
     */
    public function read() {
        $handle = fopen($this->_filename, "r"); //Открываем csv

        $this->source = array(); //Массив будет хранить данные из csv
        //Проходим весь csv-файл, и читаем построчно. 3-ий параметр разделитель поля
        while (($line = fgetcsv($handle, 0, ";")) !== FALSE) {
            $this->source[] = $line; //Записываем строчки в массив
        }
        fclose($handle); //Закрываем файл
        return true;
    }

    /**
     * Сохраняет данные в фаил $this->_filename     
     * @return boolean
     */
    public function write() {
        $handle = fopen($this->_filename, "a");

        foreach ($this->source as $value) { //Проходим массив
            //Записываем, 3-ий параметр - разделитель поля
            fputcsv($handle, explode(";", $value), ";");
        }
        fclose($handle); //Закрываем
        
        return true;
    }

    /**
     * Реализует возможность фильтрации данных 
     * @param array $params Описывает параметры выборки   
     * @example 
     *      array(
     *          'fields' => array('col1, 'col2', ...)               Должен содержать имена колонок, порядок должен соответствовать столбцам в фаиле
     *          'where'  => array('col1' => '1', 'col2' => 'test')  выборка строк с соответствующими значениями, пока оператор между аргументами AND
     *          'limit'  => 10                                      выбрать 10 строк 
     *          'limit'  => array(5,10)                             выбирает 10 строк начиная с 5        
     */
    public function find($params = array()) {
        $result = array();
        // Если в $params не задан 'column', то по дефолту колонки нумеруются обычными массивом 
        if (!empty($params['fields'])) {
            // Загоняем в $result значение из $this->source, но уже а ассоциативном массиве
            $i = 0;
            foreach ($this->source as $row) {
                $i++;
                foreach ($row as $col => $val) {
                    // Если в $params['column'] нет значение для столбца, то делаем его как col + номер его
                    if (!isset($params['fields'][$col])) {
                        $fieldName = 'col' . $col;
                    } else {
                        $fieldName = $params['fields'][$col];
                    }
                    $result[$i][$fieldName] = $val;
                }
            }

            // Выборка только нужных строк
            if (!empty($params['where'])) {
                $result = $this->where($result, $params['where']);
            }

            if (!empty($params['limit'])) {
                $result = $this->limit($result, $params['limit']);
            }
        } else {
            $result = $this->source;
        }
        return $result;
    }

    private function where($source, $params) {
        // выборка осуществляется, топорным удаление записей из result
        // что бы просто не заводить новую переменную
        foreach ($source as $i => $row) {
            
            $unset = false;
            foreach ($params as $fieldName => $fieldValue) {
                if ($row[$fieldName] != $fieldValue) {
                    $unset = true;
                } 
            }
            
            // Удаляем если строка не соотвествует запросу
            if ($unset) {
                unset($source[$i]);
            }
        }        
        return $source;
    }

    private function limit($source, $params) {        
        if (is_array($params)) {
            $source = array_slice($source, $params[0], $params[1]);
        } else {
            $source = array_slice($source, 0, $params);
        }
        return $source;
    }

    /**
     * Вставка данных
     * @param array $data
     */
    public function insert($data) {
        
        echo "----";        var_dump($data);

        $last_id = $this->source[0][0];
        $last_id++;
        
        $data['id'] = $last_id;

        foreach ($data as $fieldName => $value) {
            $this->source[] = $data;
        }       
        
        echo "----"; var_dump($this->source);
    }
    
    public function update($data) {
        
    }

    public function count() {
        return count($this->source);
    }

}
