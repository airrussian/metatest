<h1>Авторизация</h1>

<form role="form" method="post" action="<?php echo $action; ?>">
  <div class="form-group">
    <label for="userlogin">Логин:</label>
    <input type="text" class="form-control" id="userlogin" placeholder="Введите логин" name="user[login]">
  </div>
  <div class="form-group">
    <label for="userpass">Пароль</label>
    <input type="password" class="form-control" id="userpass" placeholder="Введите пароль" name="user[pass]">
  </div>
  <div class="checkbox">
    <label><input type="checkbox" name="user[remember]">Запомнить меня</label>
  </div>
  <button type="submit" class="btn btn-default">Отправить</button>
</form>