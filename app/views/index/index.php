<?php
/**
 * Думаю нормально если выведим тут md фаил со своими стилями? 
 */
$file = file_get_contents("/home/air/site/metatest/readme.md");

$file = preg_replace(array("/##\s{1}(.+?)\n/si", "/#\s{1}(.+?)\n/si"), array("<h2>$1</h2>", "<h1>$1</h1>"), $file);

echo $file;

// и подконец напишим.....
?>
<h2>Решение</h2>
<p>Решение основанно на паттернах (MVC, Singleton, Adapter)</p>

<h3>Как пользоваться?</h3>
<p>
    /app/controllers - должная содержать контроллеры обработки, 
    название файла и класса должны совпдать, у каждого класса должно прописано, 
    namespace app\controllers    
</p>

<p>
    /app/models - определяет модель для работы с данными
    namespace app\models    
</p>

<p>
    /app/views - должна содержать директорию для кажного контроллера
    namespace app\views
</p>

<p>пример: 
    /app/views/index/ - должна содержать представления для всех действий с controller 
    которые тот допусткает.         

</p>


<h3>Админские права</h3>
<p>По этой <a href="/metatest/user/auth">ссылке</a> можете авторизоваться и управлять тем, чем вам дано. </p>
<dl class='dl-horizontal'>        
    <dt>Логин:</dt>
    <dd>admin</dd>
    <dt>Пароль:</dt>
    <dd>admin</dd>
</dl>

<p>
    С уважением, Алексей Вороненко.
</p>
