<h1>Список клиентов</h1>

<table id="listClients" class="table table-hover"> 
    <thead>
        <tr>
            <th>id</th>
            <th>Имя, Фамилия</th>
            <th>Компания</th>
            <th>Должность</th>
        </tr>        
    </thead>
    <tbody>
        <?php foreach ($clients as $client) : ?>
        <tr>
            <td><?php echo $client['id']; ?></td>
            <td><a href="<?php echo \app\helpers\router::url(array('controller' => 'client', 'action' => 'view')); ?>/<?php echo $client['id']; ?>"><?php echo $client['firstName']; ?> <?php echo $client['secondName']; ?></a></td>
            <td><?php echo $client['company']; ?></td>
            <td><?php echo $client['post']; ?></td>
        </tr>                
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $paginator->show(); ?>