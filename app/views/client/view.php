<div id='userCard'>
    <div class="container">
        <div class="col-md-2 photo">
            <img src='/metatest/static/img/logo.jpg' style="width: 100%" />            
        </div>
        <div class="col-md-10">
            <h1><?php echo $client['firstName'] . " " . $client['secondName']; ?> ( <?php echo $client['birthDay']; ?> )</h1>
            <dl class='dl-horizontal'>        
                <dt>Компания:</dt>
                <dd><?php echo $client['company']; ?></dd>
                <dt>Должность:</dt>
                <dd><?php echo $client['post']; ?></dd>
                <dt>Телефон:</dt>
                <dd><a href="tel:<?php echo $client['telephone']; ?>"><?php echo $client['telephone']; ?></a></dd>                
            </dl>
        </div>
    </div>
</div>