<h1>Регистрация</h1>

<form action="<?php echo $action; ?>" id="reg" method="POST">
    <div class='container'>
        <div class='row'>
            <div class='col-sm-6'>
                <div class="input-group<?php if (isset($client['firstName']['error'])) echo " has-error"; ?>">                
                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                    <input type="text" class="form-control" placeholder="Имя" name="client[firstName][value]" required="required" value="<?php echo $client['firstName']['value']; ?>">
                </div>
            </div>
            <div class='col-sm-6'>
                <div class="input-group<?php if (isset($client['secondName']['error'])) echo " has-error"; ?>">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                    <input type="text" class="form-control" placeholder="Фамилия" name="client[secondName][value]" required="required" value="<?php echo $client['secondName']['value']; ?>">
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-sm-3'><div class="input-group<?php if (isset($client['telephone']['error'])) echo " has-error"; ?>">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    <input type="text" class="form-control" placeholder="Дата рождения" name="client[birthDay][value]" value="<?php echo $client['birthDay']['value']; ?>" pattern='\d{2}\.\d{2}\.\d{4}' />
                </div></div>
            <div class='col-sm-3'><div class="input-group<?php if (isset($client['company']['error'])) echo " has-error"; ?>">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-th-large"></span></span>
                    <input type="text" class="form-control" placeholder="Компания" name="client[company][value]" value="<?php echo $client['company']['value']; ?>" />
                </div></div>
            <div class='col-sm-3'><div class="input-group<?php if (isset($client['post']['error'])) echo " has-error"; ?>">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                    <input type="text" class="form-control" placeholder="Должность" name="client[post][value]" value="<?php echo $client['post']['value']; ?>" />
                </div>
            </div>
            <div class='col-sm-3'>
                <div class="input-group<?php if (isset($client['telephone']['error'])) echo " has-error"; ?>">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></span>
                    <input type="text" class="form-control" placeholder="Телефон" name="client[telephone][value]" required="required" value="<?php echo $client['telephone']['value']; ?>" pattern='\+7\d{5,}'                            
                            onchange="" />
                </div>    
            </div>
            <div class='row'>
                <div class="col-sm-2 col-sm-offset-5 col-xs-7 col-xs-offset-3">
                    <button type="submit" class="btn btn-primary">Регистарация</button>
                </div>
            </div>            
        </div>
    </div>
</form>