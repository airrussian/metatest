<?php

namespace app\views;

/**
 * Description of view
 *
 * @author AIR
 */

class layout {

    private $values = array();
    
    public $tmplPath;

    /**
     * Добавляет переменную для передачи её в шаблон
     * @param string $name
     * @param * $value     
     */
    public function assign($name, $value) {
        $this->values[$name] = $value;
    }

    /**
     * 
     * Возвращает текстовое значение обработки шаблона
     * @todo Весьма топорно выполнено, следует избавиться от ob_*,
     * @todo выходы:
     * @todo 1. Реализовать типо smarty с компиляцией псевдо php 
     * @todo 2. Придумать нечто совершенно гибкое XML - XLST
     * @descript метод сырой, работает. 
     * @param string $tmplName
     */
    public function fetch($tmplName = false) {           
        ob_start();        
        $this->display($tmplName);
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    }

    /**
     * @descript см. выше, только вот отличии от fetch? не нуждается в другом потоке вывода.
     * @param string $tmplName
     * @throws \Exception
     */
    public function display($tmplName = false) {
        $fileTemplates = dirname(__FILE__) . '/' . $this->tmplPath . '/' . $tmplName . '.php';
        if (file_exists($fileTemplates)) {            
            if (!empty($this->values)) {
                foreach ($this->values as $key => $value) {
                    ${$key} = $value;
                }
            }
            require $fileTemplates;                                    
        } else {
            throw new \Exception('Шаблон не найден' . $fileTemplates);
        }
    }

}
