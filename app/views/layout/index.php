<!DOCUMENT type="html">
<html>
    <head>                
        <title><?php echo $document['title']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo $document['url']['static']; ?>/bootstrap/css/bootstrap.min.css" />

        <link rel="stylesheet" href="<?php echo $document['url']['static']; ?>/main.css" />

        <script src="<?php echo $document['url']['static']; ?>/jquery-1.11.2.min.js"></script>
        <script src="<?php echo $document['url']['static']; ?>/bootstrap/js/bootstrap.min.js"></script>

        <script src="<?php echo $document['url']['static']; ?>/main.js"></script>               
    </head>
    <body>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 col-sm-1">
                        <a href="/metatest/" id="logo"><img src="<?php echo $document['url']['static']; ?>/img/logo.jpg" alt="<?php echo $site['name']; ?>" class="img-circle" /></a>
                    </div>
                    <div class="col-xs-7 col-sm-7">
                        <a href="https://bitbucket.org/johancode/metatest/overview" target="_blank">Тестовое задание</a> для Метадизайн
                    </div>
                    <div class="col-xs-2 col-sm-4">
                        <?php echo $userbar; ?>
                    </div>
                </div>
            </div>
        </header>
        <nav>
            <div class="container">
                <?php echo $menu; ?>
            </div>            
        </nav>
        <section id="main">
            <div class="container">
                <?php echo $content; ?>
            </div>            
        </section>
        <footer>
            <div class="container">                
                <div>
                    <span class="phone">Телефон: <a href="tel:+79233248601">+79233248601</a></span>
                    <span class="email">Email: <a href="mailto:airrusian@mail.ru">airrussian@mail.ru</a></span>
                </div>  
            </div>
        </footer>

    </body>
</html>

