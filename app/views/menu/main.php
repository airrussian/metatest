<ul class="nav nav-pills">
    <?php foreach ($menu as $li => $value) : ?>
        <li class="item">
            <a href="<?php echo $value['url']; ?>"><?php echo $value['name']; ?></a>
        </li>
    <?php endforeach; ?>
</ul>