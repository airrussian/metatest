<?php

namespace app;

require_once dirname(__FILE__) . '/main.php';

try {
    $layout = new views\layout();

    // Это нужно как-то загрузить из config и ещё и что бы можно 
    // было переопределять данные после вызова controller
    $document = array(
        'title' => 'Страница 1',
        'url' => array(
            'static' => 'http://airrussian.ru/metatest/static'
        )
    );

    $page = route();

    // CONTENT 
    $controller = "\\app\\controllers\\" . $page['controller'];
    $action = $page['action'];
    $user = new $controller();
    $result = $user->$action($page['param']);
    $layout->assign('content', $result);
    // END CONTENT
    
    // START MODULES
    $modules = array(
        'menu' => array('controller' => 'menu', 'action' => 'main'),
        'userbar'   =>  array('controller' => 'user', 'action' => 'bar')
    );

    foreach ($modules as $modName => $page) {
        $controller = "\\app\\controllers\\" . $page['controller'];
        $action = $page['action'];
        $user = new $controller();
        $result = $user->$action($page['param']);
        $layout->assign($modName, $result);
    }

    // END MODULES

    $layout->assign('document', $document);

    $layout->display('layout/index');
} catch (\Exception $ex) {
    echo $ex->getMessage();
}

